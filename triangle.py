# Accepts the number of iterations u like to go with
# Accetps the character you want to build the triangle with
# Didn't use the padding since that won't give the nice look to triangle
# If you like to use padding, just replace 2 instances of " " in print section (line 13) 
# inside the for loop with whatever characters u like 
# Plese read the README.md file for the logic
def triangle():
    iteration = int(input("How many iterations u want:"))
    input_char = str(input("Which character u want to build the Triangle:"))
    j=1
    print("No of iterations u like to go: {0}".format(iteration))
    for i in range(1,iteration+1,1):
        print(" "*(iteration-i)+input_char*(j)+" "*(iteration-i))
        j=j+2

triangle()