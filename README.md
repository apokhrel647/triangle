******************

CREATE A TRIANGLE WITH PYTHON

*****************

Simple logic

For different iterations, observe how the values are increasing and decreasing

For 1,3,5,7,9 : use another variable j and increase by 2

For iteration-1..5, just use i

i=1:    ****1****   (iteration-1)*"0" + 1*"1" + (iteration-1)*"0"     

i=2:    ***111***   (iteration-2)*"0" + 3*"1" + (iteration-2)*"0"

i=3:    **11111**   (iteration-3)*"0" + 5*"1" + (iteration-3)*"0"

i=4:    *1111111*   (iteration-4)*"0" + 7*"1" + (iteration-4)*"0"

i=5:    111111111   (iteration-5)*"0" + 9*"1" + (iteration-5)*"0"


 